from flask import Flask
from flask import request, jsonify,json
from flask import render_template
from flask import Flask
from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp
from flask_restful import Resource, Api


#Import
import datetime
import psycopg2


#llamando funciones de diferentes archivos
from lider import User,authenticate,identity
from conexion import insert_vot,select_users,municipios_cantidad,cantidad_puntos,votantes_lideres
from coordenadas import coordenadas



#Configuracion iniciar
app = Flask(__name__)
app.config['SECRET_KEY'] = 'super-secret'
api = Api(app)

#Clase principal
class index(Resource):
    def get(self):
        return {'hello': 'world'}
#------------------------------------------------Configuracion de rutas para validacion del JWT--------------------------
    jwt = JWT(app, authenticate, identity)
    @app.route('/protected')
    @jwt_required()
    def protected():
        return '%s' % current_identity
#------------------------------------------------------Ruta para crear los votantes----------------------------------------
    @app.route('/crear/votante',  methods = ['POST'])
    @jwt_required()
    def task():
        nombre_votante = request.json['Nombre']
        apellido_votante = request.json['Apellido']
        direccion_votante = request.json['Direccion']
        telefono_votante = int(request.json['Telefono'])
        cc_votante = int(request.json['Cedula'])
        id_lider = int(request.json['liderid'])
        id_puesto = int(request.json['id_puesto'])
        #validacion de cantidad de datos JSON
        count = request.json
        if len(count) > 7 :
            response = 'Datos incorrectos en el JSON'
        try:
            entero = int(cc_votante and id_puesto)
            response = "Datos insertados correctamente"
            conexion = psycopg2.connect(host="localhost", database="back_test", user="postgres", password="admin")
            cursor=conexion.cursor()
            cursor.execute("SELECT version();")
            record = cursor.fetchone()
            #Insert
            postgres_insert_query = """ INSERT INTO votante (nombres, apellidos, municipio, telefono,cedula, lider_id, id_puesto, fecha, status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
            record_to_insert = (nombre_votante,apellido_votante, direccion_votante,telefono_votante,cc_votante,id_lider,id_puesto,datetime.datetime.now(),"Activo")
            cursor.execute(postgres_insert_query, record_to_insert)
            conexion.commit()
            count = cursor.rowcount
            response = "Datos insertados correctamente en la tabla votante"
            return response
            print(cc_votante)
        except ValueError:
            response = "Datos incorrectos en el JSON"

        return response
#------------------------------------------------------------Ruta para definir la cantidad de votantes en el sistema----------------------------------------
    @app.route('/cantidad/votante',  methods = ['GET'])
    @jwt_required()
    def vontantes():
        cantidad = select_users()
        print (cantidad)
        return "La cantidad de votantes inscritos en el sistema es  : {}".format(cantidad)
#-----------------------------------------------------------Ruta para definir la cantidad de votantes por municipios----------------------------------------
    @app.route('/cantidad/municipios',  methods = ['GET'])
    @jwt_required()
    def municipios():
        cantidad = municipios_cantidad()
        print ("CANTIDAAAAAA" ,cantidad)
        return "La cantidad de municipios inscritos en el sistema es  :\n {}".format(cantidad)
#-----------------------------------------------------------Ruta para definir la cantidad de votantes por mesa de votacion----------------------------------------
    @app.route('/cantidad/votacion',  methods = ['GET'])
    @jwt_required()
    def mesas_votacion():
        result = cantidad_puntos()
        return "La cantidad de mesas inscritas por persona en el sistema son  :\n {}".format(result)
#-----------------------------------------------Rutas para definir la cantidad de votantes inscritos por lider----------------------------------------
    @app.route('/votantes/lider',  methods = ['GET'])
    @jwt_required()
    def calcular_votantes():
        result = votantes_lideres()
        return result
#-----------------------------------------------Rutas para obtener coordenadas segun datos ingresados----------------------------------------
    @app.route('/coordenadas', methods = ['POST'])
    @jwt_required()
    def coordenadas():
        direccion = request.json['Direccion']
        municipio = request.json['Municipio']

        params = coordenadas(direccion, municipio)

        return params



api.add_resource(index, '/')
if __name__ == '__main__':
    app.run(debug = True , port= 5004)
