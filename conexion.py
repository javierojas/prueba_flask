import psycopg2
import json
import datetime


conexion = psycopg2.connect(host="localhost", database="back_test", user="postgres", password="admin")
cursor=conexion.cursor()
cursor.execute("SELECT version();")
record = cursor.fetchone()


#----------------------------------------------------------SELECT USUARIOS--------------------------------------------------
def select_users():
    query_select= "select * from users"
    cursor.execute(query_select)
    mobiler_records = cursor.fetchall()

    name = ''
    for rows in mobiler_records :
         name = rows[1]
#---------------------------------------------------------INSERT VOTANTES-----------------------------------------------------
def insert_vot(nombres,apellidos,direccion,telefono,cedula,lider_id,id_puesto,fecha,status):
    cursor=conexion.cursor()
    postgres_insert_query = """ INSERT INTO votante (nombres, apellidos, direccion, telefono,cedula, lider_id, id_puesto, fecha, status) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    record_to_insert = ("{}, {}, {}, {}, {}, {} ,{} , {}, {}".format(nombres,apellidos,direccion,telefono,cedula,lider_id,id_puesto,fecha,status))
    cursor.execute(postgres_insert_query, [record_to_insert])
    conexion.commit()
    count = cursor.rowcount
    return print("Datos insertados correctamente en la tabla votante")
#---------------------------------------------------------CANTIDAD DE VOTANTES EN EL SISTEMA-----------------------------------------------------
def select_users():
    query_select= "select * from votante"
    cursor.execute(query_select)
    mobiler_records = cursor.fetchall()

    votantes = ''
    for rows in mobiler_records :
        votantes = rows[9]
    return votantes
#---------------------------------------------------------CANTIDAD DE VOTANTES EN EL SISTEMA POR MUNICIPIO----------------------------------------------------
def municipios_cantidad():
    query_select= "select * from votante"
    cursor.execute(query_select)
    mobiler_records = cursor.fetchall()

    cantidad = []
    for rows in mobiler_records :
        mun = rows[2]
        cantidad.append(mun)
        list_mun = '\n'.join(map(str, cantidad))

    return list_mun
#---------------------------------------------------------CANTIDAD DE VOTANTES POR MESA DE VOTACION----------------------------------------------------
def cantidad_puntos():
    query_select= "SELECT puntos_votacion.nombre, puntos_votacion.id_puesto , votante.nombres FROM puntos_votacion INNER JOIN votante ON Votante.id_puesto = puntos_votacion.id_puesto;"
    cursor.execute(query_select)
    mobiler_records = cursor.fetchall()

    resultV = []
    resultM = []
    resultID = []
    for rows in mobiler_records :
        nombre_puesto = rows[0]
        resultM.append(nombre_puesto)
        result_name = '\n'.join(map(str, resultM))
    return result_name
#---------------------------------------------------------CANTIDAD DE VOTANTES CREADO POR LIDER----------------------------------------------------
def votantes_lideres():
    query_select= "SELECT users.id, users.name , votante.nombres FROM users INNER JOIN votante ON Votante.lider_id = users.id WHERE users.id = 1;"
    cursor.execute(query_select)
    mobiler_records = cursor.fetchall()

    cantidad_usuarios = len(mobiler_records)
    message = "La cantidad de usuarios para el lider con ID 1 es : {}".format(cantidad_usuarios)


    query_select= "SELECT users.id, users.name , votante.nombres FROM users INNER JOIN votante ON Votante.lider_id = users.id WHERE users.id = 2;"
    cursor.execute(query_select)
    mobiler_records = cursor.fetchall()

    cantidad = len(mobiler_records)

    messages = "La cantidad de usuarios para el lider con ID 2 es : {}".format(cantidad)

    response = message + "\n" + messages
    return response

