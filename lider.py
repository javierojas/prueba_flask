from flask import Flask
from flask import request, jsonify
from flask import render_template
from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp 
from conexion import  select_users


#------------------------------------------------------------------------------------------------------------------
#------------------------------------------------LOGIN-------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------
#Autenticacion por medio de JWT
class User(object):
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id
#Usuarios
users = [
    User(1, 'Javier', 'Test2021*'),
    User(2, 'Julian', 'Test2021*'),
]
username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}

def authenticate(username, password):
    user = username_table.get(username, None)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user

def identity(payload):
    user_id = payload['identity']
    return userid_table.get(user_id, None)

