# README #

Prueba tecnica back_flask

### How do I get set up? ###

* Activar el entorno virtual 'entorno'
* RUN pip install -r requirements.txt
* Configurar los accesos a las bases localmente en los archivos conexion y main
* RUN python3 'main.py'


### warnings ###

* Para consultar los servicios se requieren de un token-acces, que se obtiene logueandose
* Si el sistema se apaga, volver a generar el token
* El token expira cuando pasa un lapso de tiempo largo
* Cuando consulte las coordenadas envie direcciones correctas


### Who do I talk to? ###

* @JAVIEROJAS